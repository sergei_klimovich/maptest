//
//  ContentView.swift
//  MapTest
//
//  Created by Администратор on 09.12.2020.
//

import SwiftUI
import MapKit

struct ContentView: View {
    var body: some View {
        Home()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class locatDelegate: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published var pins : [Pin] = []
    @Published var region : MKCoordinateRegion = MKCoordinateRegion()
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.pins.append(Pin(location: locations.last!))
        self.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude), latitudinalMeters: 10000, longitudinalMeters: 10000)
    }
}

struct Home: View {
//    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 52, longitude: 37), latitudinalMeters: 10000, longitudinalMeters: 10000)
    @State var manager = CLLocationManager()
    @StateObject var deleg = locatDelegate()
    
    @State var tracking : MapUserTrackingMode = .follow
    
    var body: some View {
        VStack{
            Map(coordinateRegion: $deleg.region, interactionModes: .all, showsUserLocation: true, userTrackingMode: $tracking, annotationItems: deleg.pins) { item in
//                MapPin(coordinate: item.location.coordinate, tint: .red)
                MapAnnotation(coordinate: item.location.coordinate) {
                    Image(systemName: "Map").frame(width: 100, height: 100, alignment: .center)
                }
            }
            
        } .onAppear(perform: {
            
            self.manager.delegate = self.deleg
            self.manager.startUpdatingLocation()
            
        })
    }
}

struct Pin: Identifiable {
    var id = UUID().uuidString
    var location: CLLocation
}
